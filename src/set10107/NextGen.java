package set10107;

import java.util.Collections;
import java.util.Random;

import modelP.JSSP;
import modelP.Problem;

public class NextGen {
	private  int populationSize;
	private Problem problem;
	private  int[][][] population;

	public NextGen(int _populationSize, Problem _problem, int[][][] _population){
		populationSize = _populationSize;
		problem = _problem;
		population = _population;
	}

	public int[][][] nextGen(){
		int[][][] nextGenPop = new int[populationSize][problem.getNumberOfMachines()][problem.getNumberOfJobs()];

		int[][] min = population[0];
		int[][] secondMin = population[1];
		int[][] thirdMin = population[2];
		int[][] fourthMin = population[3];


		//find the solutions with the best 4 fitnesses and add them to the new generation
		for (int i = 0; i < population.length; i++)
		{
			if (JSSP.getFitness(population[i], problem) < JSSP.getFitness(min, problem))
			{
				secondMin = min;
				min = population[i];
			}
			else if(JSSP.getFitness(population[i], problem) < JSSP.getFitness(secondMin, problem) && JSSP.getFitness(population[i], problem) > JSSP.getFitness(min, problem))
			{
				secondMin = population[i];
			}
			else if(JSSP.getFitness(population[i], problem) < JSSP.getFitness(thirdMin, problem) && JSSP.getFitness(population[i], problem) > JSSP.getFitness(secondMin, problem))
			{
				thirdMin = population[i];
			}
			else if(JSSP.getFitness(population[i], problem) < JSSP.getFitness(fourthMin, problem) && JSSP.getFitness(population[i], problem) > JSSP.getFitness(thirdMin, problem))
			{
				fourthMin = population[i];
			}

		}
		//the first 4 values of the new population are taken from the best 4 values in the previous generation(elitism)
		nextGenPop[0] = min;
		nextGenPop[1] = secondMin;
		nextGenPop[2] = thirdMin;
		nextGenPop[3] = fourthMin;

		//		for(int i = 4; i < 10; i++){
		//			nextGenPop[i] = JSSP.getRandomSolution(problem);
		//		}
		//create 100 children from previous population and insert into new population
		for(int i = 4; i < populationSize; i+=2){

			int[][][] children = generateChildren(populationSize, problem, population);
			nextGenPop[i] = children[0];
			nextGenPop[i+1] = children[1];
		}
		return nextGenPop;
		//	return population;
	}
	public int[][][] generateChildren(int populationSize, Problem problem, int[][][] population)
	{

		//get parent 1
		int parent1[][] = TournamentSelection(populationSize, problem, population);
		// get parent 2
		int parent2[][] = TournamentSelection(populationSize, problem, population);
		//create empty child array
		int child1[][] = new int[problem.getNumberOfMachines()][problem.getNumberOfJobs()];
		int child2[][] = new int[problem.getNumberOfMachines()][problem.getNumberOfJobs()];



		//One Point Crossover
		//************************************************************************************
		// pick cut point
		Random randomGenerator = new Random();
		int split = randomGenerator.nextInt(parent1.length);
		while(split == 0 || split == parent1.length){
			split = randomGenerator.nextInt(parent1.length);
		}
		//				//create empty child array
		//				int child1[][] = new int[problem.getNumberOfMachines()][problem.getNumberOfJobs()];
		//				int child2[][] = new int[problem.getNumberOfMachines()][problem.getNumberOfJobs()];

		for(int j = 0; j < problem.getNumberOfJobs(); j++){
			// genes from parent1
			for (int i=0; i < split; i++){
				child1[i][j] = parent1[i][j];
			}
			// and genes from parent2
			for (int i=split; i < problem.getNumberOfMachines(); i++){
				child1[i][j] = parent2[i][j];
			}

			// genes from parent1
			for (int i=0; i < split; i++){
				child2[i][j] = parent2[i][j];
			}

			// and genes from parent2
			for (int i=split; i < problem.getNumberOfMachines(); i++){
				child2[i][j] = parent1[i][j];
			}

		}
		//************************************************************************************
		//mutate the children
		Mutation(child1, problem);
		Mutation(child2, problem);
		//return both children
		int[][][] children = new int[2][problem.getNumberOfMachines()][problem.getNumberOfJobs()];
		children[0] = child1;
		children[1] = child2;
		return children;
	}

	public static void Mutation(int[][] solution, Problem problem)
	{
		Random randomGenerator = new Random();
		double mutationRate = 0.1;

		int point1 = randomGenerator.nextInt(solution.length);
		int point2 = randomGenerator.nextInt(solution.length);
		for(int i = 0; i < problem.getNumberOfMachines(); i++){
			if(randomGenerator.nextDouble() < mutationRate){
				int temp = solution[i][point1];
				solution[i][point1] = solution[i][point2];
				solution[i][point2] = temp;
			}
		}
	}


	public static int[][] TournamentSelection(int populationSize, Problem problem, int[][][] population){
		int sample = populationSize/7;
		int[][][] newpopulation = new int[sample][problem.getNumberOfMachines()][problem.getNumberOfJobs()];

		int[][] chosenparent = new int[problem.getNumberOfMachines()][problem.getNumberOfJobs()];
		Random randomGenerator = new Random();
		//select solutions at random from population
		for(int i = 0; i < sample; i++)
		{
			newpopulation[i] = population[randomGenerator.nextInt(populationSize)];
		}
		//get the best fitness in new population
		int bestfitness = JSSP.getFitness(newpopulation[0], problem);
		chosenparent = newpopulation[0];
		for(int i = 0; i < sample; i++)
		{
			if(JSSP.getFitness(newpopulation[i], problem) < bestfitness)
			{
				bestfitness = JSSP.getFitness(newpopulation[i], problem);
				chosenparent = newpopulation[i];
			}
		}
		return chosenparent;
	}



}
