package set10107;

import modelP.JSSP;
import modelP.Problem;

public class FinalSolution {

	public static void main(String[] args) {
		int problemNo = 10;
		Problem problem = JSSP.getProblem(problemNo);		
		//print problem to std.out
		JSSP.printProblem(problem);
		int[][] bestSolution = new int[problem.getNumberOfMachines()][problem.getNumberOfJobs()];
		//generate 100 random solutions
		int populationSize = 200;
		//number of generations;
		int generations = 200;
		int[][][] population = new int[populationSize][problem.getNumberOfMachines()][problem.getNumberOfJobs()];
		for(int i = 0; i < populationSize; i++){
			population[i] = JSSP.getRandomSolution(problem);
		}

		//find the best fitness 
		int[][] min = population[0];

		for(int i = 0; i < populationSize; i++)
		{
			if (JSSP.getFitness(min, problem) > JSSP.getFitness(population[i], problem)){
				min = population[i];
			}
		}
		int[][] oldmin = min;

		System.out.println("Starting fitness: " + JSSP.getFitness(min, problem));
		for(int j = 0; j < generations; j++){
			NextGen nextgen = new NextGen(populationSize, problem, population);
			population = nextgen.nextGen();


			int[][] min2 = population[0];
			oldmin = min2;
			for(int i = 0; i < populationSize; i++)
			{
				if (JSSP.getFitness(min2, problem) > JSSP.getFitness(population[i], problem)){
					min2 = population[i];
				}
			}

			bestSolution = min2;
			//	if(oldmin != min2){
			System.out.println(/*"Generation " + (j + 1) + ": " +*/ JSSP.getFitness(min2, problem));
			//JSSP.printSolution(min2, problem);
			//	}
		}

		System.out.println("optimum fitness: " + problem.getLowerBound());

		/**
		 * Save the solution (defaults to the project directory saving to a .txt file prefixed with the current computer time in milliseconds) 
		 * The filename is returned
		 */
		String filename = JSSP.saveSolution(bestSolution, problem);

		/**
		 * load solution from default directory
		 */
		int[][] solution2 = JSSP.loadSolution(filename); 

		/**
		 * get the problem Id from the saved solution 
		 */
		int id = JSSP.getProblemIdFromSolution(filename);
		/**
		 * load the associated problem
		 */
		Problem problem2 = JSSP.getProblem(id);

		/**
		 * Check the solution and print to std.out 
		 */
		JSSP.printSolution(solution2, problem2);


		/**
		 * Display a saved solution graphically 
		 */
		JSSP.displaySolution(filename);	
	}
}
